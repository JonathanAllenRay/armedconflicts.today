from flask import Flask
from flask import jsonify
import json
#from flask_sqlalchemy import SQLAlchemy
#from sqlalchemy.ext.automap import automap_base
import sys
import psycopg2
from flask_cors import CORS
from flask import request

app = Flask(__name__)
CORS(app)

def qconn() :
    return psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')

# Function code borrowed from: https://stackoverflow.com/questions/3286525/return-sql-table-as-json-in-python
def query_db(db, query, args=(), one=False):
    cur = db.cursor()
    cur.execute(query % args)
    r = [dict((cur.description[i][0], value) \
               for i, value in enumerate(row)) for row in cur.fetchall()]
    cur.connection.close()
    return (r[0] if r else None) if one else r

@app.route('/')
def hello_world():
  return 'Hello from Flask!'

# For other devs reading this, see https://stackoverflow.com/questions/15182696/multiple-parameters-in-in-flask-approute
# for help on using query params
# When /conflicts?conflict_demo=blah&stuff=yep is searched, will print blah and yep and return all conflicts 
# For other devs reading this, see https://stackoverflow.com/questions/15182696/multiple-parameters-in-in-flask-approute
# for help on using query params
# When /conflicts?conflict=blah&stuff=yep is searched, will print blah and yep and return all conflicts 
@app.route('/conflicts', methods=['GET'])
def get_all_conflicts():
    num_results = request.args.get('results', None)
    offset = request.args.get('offset', None)
    conn = qconn()
    query_result = None
    if (num_results != None and offset != None) :
        query_result = query_db(conn, "select * from conflicts LIMIT %s OFFSET %s", (num_results, offset,))
    else :
        query_result = query_db(conn, "select * from conflicts")
    response = query_result
    return jsonify(response)

@app.route('/conflicts/<conflict_id>', methods=['GET'])
def get_conflict_by_id(conflict_id):
    conn = qconn()
    query_result = query_db(conn, "select * from conflicts where id = %s", (conflict_id,))
    response = query_result
    return jsonify(response)




@app.route('/countries', methods=['GET'])
def get_all_countries():
    num_results = request.args.get('results', None)
    offset = request.args.get('offset', None)
    conn = qconn()
    query_result = None
    if (num_results != None and offset != None) :
        query_result = query_db(conn, "select * from countries LIMIT %s OFFSET %s", (num_results, offset,))
    else :
        query_result = query_db(conn, "select * from countries")
    response = query_result
    return jsonify(response)

@app.route('/countries/<country_id>', methods=['GET'])
def get_country_by_id(country_id):
    conn = qconn()
    query_result = query_db(conn, "select * from countries where id = %s", (country_id,))
    response = query_result
    return jsonify(response)

@app.route('/countries/name/<name>', methods=['GET'])
def get_country_by_name(name):
    conn = qconn()
    query_result = query_db(conn, "select * from countries where name = \'%s\'", (name,))
    response = query_result
    return jsonify(response)


@app.route('/news', methods=['GET'])
def get_all_news():
    num_results = request.args.get('results', None)
    offset = request.args.get('offset', None)
    conn = qconn()
    query_result = None
    if (num_results != None and offset != None) :
        query_result = query_db(conn, "select * from news LIMIT %s OFFSET %s", (num_results, offset,))
    else :
        query_result = query_db(conn, "select * from news")
    response = query_result
    return jsonify(response)

@app.route('/news/<news_id>', methods=['GET'])
def get_news_by_id(news_id):
    conn = qconn()
    query_result = query_db(conn, "select * from news where id = %s", (news_id,))
    response = query_result
    return jsonify(response)

@app.route('/news/conflict/<conflict_id>', methods=['GET'])
def get_news_by_conflict_id(conflict_id):
    num_results = request.args.get('results', None)
    offset = request.args.get('offset', None)
    conn = qconn()
    query_result = None
    if (num_results != None and offset != None) :
        query_result = query_db(conn, "select * from news where related_conflict_id = %s LIMIT %s OFFSET %s", (conflict_id, num_results, offset,))
    else :
        query_result = query_db(conn, "select * from news where related_conflict_id = %s", (conflict_id,))
    response = query_result
    return jsonify(response)

@app.route('/count/<table>', methods=['GET'])
def count_table(table):
    conn = qconn()
    query_result = query_db(conn, "SELECT COUNT(*) FROM %s", (table,))
    response = query_result
    return jsonify(response)

@app.route('/countries/conflict/<conflict_id>', methods=['GET'])
def get_countries_with_conflict_id(conflict_id):
    conn = qconn()
    query_result = None
    sql_string = "select related_countries from conflicts where id = %s"
    response = query_db(conn, sql_string, (conflict_id,))
    countries = response[0].get('related_countries')
    sql_string = "select * from countries where name in ("
    for country in countries :
        sql_string += '\'' + country + '\', '
    sql_string += '\'end\')'
    conn = qconn()
    response2 = jsonify(query_db(conn, sql_string))
    return response2

@app.route('/conflicts/country/<country_name>', methods=['GET'])
def get_conflicts_with_country_name(country_name):
    conn = qconn()
    query_result = None
    sql_string = "select related_conflicts from countries where name = \'%s\'"
    response = query_db(conn, sql_string, (country_name,))
    conflicts = response[0].get('related_conflicts')
    sql_string = "select * from conflicts where id in ("
    for conflict_id in conflicts :
        sql_string += '\'' + str(conflict_id) + '\', '
    sql_string += '\'9999\')'
    conn = qconn()
    response2 = jsonify(query_db(conn, sql_string))
    return response2



if __name__ == '__main__':
    app.run(port=3003, debug=True)

# Below is stuff that might be used later, but probably not
# db_string = "postgres://armedconflicts:dripharder@armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com:5432/ArmedConflictsDB "




