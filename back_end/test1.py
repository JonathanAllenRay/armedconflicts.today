import os
import main
import unittest
import tempfile
import json

def get_file(file_name):
    f = open(file_name, 'r')
    val = eval(f.readlines()[0])
    f.close()
    return val

def find_index(l, key, val):
    for i in range(len(l)):
        if l[i][key] == val:
            return i

def find_list_items(l, key, val):
    vals = []
    for item in l:
        if type(item[key]) == type(val):
            if item[key] == val:
                vals.append(item)
        elif val in item[key]:
            vals.append(item)
    return vals

class FlaskrTestCase(unittest.TestCase):

    def setUp(self):
        main.app.testing = True
        self.app = main.app.test_client()
        self.status_ok = '200 OK'
        self.conflicts = get_file("conflicts.txt")
        self.countries = get_file("countries.txt")
        self.news = get_file("news.txt")
    
    #get /countries
    def test_empty_db(self):
        rv = self.app.get('/conflicts')
        data = json.loads(rv.data)
        self.assertEqual(len(data), 59)
        self.assertEqual(data, self.conflicts)
        self.assertEqual(rv.status, self.status_ok)

    def test_conflict_id_zero(self):
        rv = self.app.get('conflicts/0')
        data = json.loads(rv.data)
        i = find_index(self.conflicts, 'id', 0)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0], self.conflicts[i])
        self.assertEqual(rv.status, self.status_ok)

    #get /conflicts/<conflict_id>
    def test_conflict_id(self):
        rv = self.app.get('/conflicts/1')
        data = json.loads(rv.data)
        i = find_index(self.conflicts, 'id', 1)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0], self.conflicts[i])
        self.assertEqual(rv.status, self.status_ok)

    #get /countries
    def test_countries(self):
        rv = self.app.get('/countries')
        data = json.loads(rv.data)
        self.assertEqual(len(data), 40)
        self.assertEqual(data, self.countries)
        self.assertEqual(rv.status, self.status_ok)

    def test_countries_country_id_zero(self):
        rv = self.app.get('/countries/0')
        data = json.loads(rv.data)
        i = find_index(self.countries, 'id', 0)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0], self.countries[i])
        self.assertEqual(rv.status, self.status_ok)

    #get /countries/<country_id>
    def test_countries_country_id(self):
        rv = self.app.get('/countries/3')
        data = json.loads(rv.data)
        i = find_index(self.countries, 'id', 3)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0], self.countries[i])
        self.assertEqual(rv.status, self.status_ok)

    #get /countries/name/<country_name>
    def test_countries_name_country_name(self):
        rv = self.app.get('/countries/name/Iraq')
        data = json.loads(rv.data)
        i = find_index(self.countries, 'name', "Iraq")
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0], self.countries[i])
        self.assertEqual(rv.status, self.status_ok)

    #get /news
    def test_news(self):
        rv = self.app.get('/news')
        data = json.loads(rv.data)
        self.assertEqual(len(data), 734)
        self.assertEqual(data, self.news)
        self.assertEqual(rv.status, self.status_ok)

    #get /news/<news_id>
    def test_news_news_id(self):
        rv = self.app.get('/news/1')
        data = json.loads(rv.data)
        i = find_index(self.news, 'id', 1)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0], self.news[i])
        self.assertEqual(rv.status, self.status_ok)

    #get /news/conflict/<conflict_id>
    def test_news_conflict_conflict_id(self):
        rv = self.app.get('/news/conflict/1')
        data = json.loads(rv.data)
        related = find_list_items(self.news, 'related_conflict_id', 1)
        self.assertEqual(len(data), 20)
        self.assertEqual(len(data), len(related))
        self.assertEqual(data, related)
        self.assertEqual(rv.status, self.status_ok)

    #get /count/<table>
    #def test_count_table(self):
    #    rv = self.app.get('/count/table')
    #    data = json.loads(rv.data)
    #    self.assertGreater(len(data), 0)

    #get /countries/conflict/<conflict_id>
    def test_countries_conflict_conflict_id(self):
        rv = self.app.get('/countries/conflict/1')
        data = json.loads(rv.data)
        related = find_list_items(self.countries, 'related_conflicts', 1)
        self.assertEqual(len(data), 1)
        self.assertEqual(data, related)
        self.assertEqual(rv.status, self.status_ok)

    #get /conflicts/country/<country_name>
    def test_conflicts_country_country_name(self):
        rv = self.app.get('conflicts/country/Iraq')
        data = json.loads(rv.data)
        related = find_list_items(self.conflicts, 'related_countries', "Iraq")
        self.assertEqual(len(data), 2)
        self.assertEqual(data, related)
        self.assertEqual(rv.status, self.status_ok)

if __name__ == '__main__':
    unittest.main()
