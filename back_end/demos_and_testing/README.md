# Note that this is not a folder for acceptance/unit tests.
# It is intended as a "playground" area for trying out bits
# of code we intend to re-use or implement later.