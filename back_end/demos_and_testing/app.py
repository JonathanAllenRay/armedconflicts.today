from flask import Flask
from flask import jsonify
#from flask_sqlalchemy import SQLAlchemy
#from sqlalchemy.ext.automap import automap_base
import sys
import psycopg2
from flask import request

app = Flask(__name__)

def qconn() :
    return psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')

# Function code borrowed from: https://stackoverflow.com/questions/3286525/return-sql-table-as-json-in-python
def query_db(db, query, args=(), one=False):
    cur = db.cursor()
    cur.execute(query % args)
    r = [dict((cur.description[i][0], value) \
               for i, value in enumerate(row)) for row in cur.fetchall()]
    cur.connection.close()
    return (r[0] if r else None) if one else r

# For other devs reading this, see https://stackoverflow.com/questions/15182696/multiple-parameters-in-in-flask-approute
# for help on using query params
# When /conflicts?conflict_demo=blah&stuff=yep is searched, will print blah and yep and return all conflicts 
# For other devs reading this, see https://stackoverflow.com/questions/15182696/multiple-parameters-in-in-flask-approute
# for help on using query params
# When /conflicts?conflict=blah&stuff=yep is searched, will print blah and yep and return all conflicts 
@app.route('/conflicts', methods=['GET'])
def demo_conflicts():
    conn = qconn()
    query_result = query_db(conn, "select * from conflicts")
    response = query_result
    conflict  = request.args.get('conflict', None)
    stuff  = request.args.get('stuff', None)
    print("Received call from front-end.")
    return jsonify(response)


@app.route('/conflicts', methods=['GET'])
def demo_conflicts2():
    conn = qconn()
    conflict  = request.args.get('conflict', None)
    # Have to use this method because for some unknown reason,
    # attemping to use execute with tuple parameters adds extra
    # quotation marks which break the query
    query_result = query_db(conn, "select * from %s", (conflict,))
    response = query_result
    return jsonify(response)


if __name__ == '__main__':
    app.run(port=4996, debug=True)

# Below is stuff that might be used later, but probably not
# db_string = "postgres://armedconflicts:dripharder@armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com:5432/ArmedConflictsDB "




