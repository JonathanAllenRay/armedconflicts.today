import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC 
import os

class SeleniumTest(unittest.TestCase):

    def setUp(self):
        cwd = os.getcwd()
        path = cwd + "/chromedriver"
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(options=chrome_options, executable_path=path)


    #Dana
    #title should be Armed Conflicts Today
    def test_title(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        self.assertEqual("Armed Conflicts Today", driver.title)

    #Dana
    #should be about button in nav bar
    def test_about_button(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        element = driver.find_element_by_link_text("About")
        element.click()
        self.assertEqual("http://armedconflicts.today/#/About", driver.current_url)

    #Dana
    #should be conflicts button in nav bar
    def test_conflicts_button(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        element = driver.find_element_by_link_text("Conflicts")
        element.click()
        self.assertEqual("http://armedconflicts.today/#/Conflicts", driver.current_url)

    #Dana
    #should be countries button in nav bar
    def test_countries_button(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        element = driver.find_element_by_link_text("Countries")
        element.click()
        self.assertEqual("http://armedconflicts.today/#/Countries", driver.current_url)

    #Dana
    #should be news button in nav bar
    def test_news_button(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        element = driver.find_element_by_link_text("News")
        element.click()
        self.assertEqual("http://armedconflicts.today/#/News", driver.current_url)

    #Dana
    #should be test button in nav bar
    def test_search_button(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        element = driver.find_element_by_link_text("Search")
        element.click()
        self.assertEqual("http://armedconflicts.today/#/Search", driver.current_url)

    #Dana
    #should be 6 different cards in about page each for 1 person
    def test_about_numbers(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        element = driver.find_element_by_link_text("About")
        element.click()
        driver.implicitly_wait(3)
        cards = self.driver.find_elements_by_class_name('col-sm-4')
        self.assertEqual(6, len(cards))

    #Dana
    #should be at least 3 reults in countries page
    def test_countries_result_numbers(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        element = driver.find_element_by_link_text("Countries")
        element.click()
        driver.implicitly_wait(3)
        cards = self.driver.find_elements_by_class_name('col-sm-4')
        self.assertGreaterEqual(len(cards), 3)  


    #Dana
    #should be at least 3 reults in conflicts page
    def test_conflicts_result_numbers(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        element = driver.find_element_by_link_text("Conflicts")
        element.click()
        driver.implicitly_wait(3)
        cards = self.driver.find_elements_by_class_name('col-sm-4')
        self.assertGreaterEqual(len(cards), 3)     


    #Dana
    #should be at least 3 reults in conflicts page
    def test_news_result_numbers(self):
        driver = self.driver
        driver.get("http://armedconflicts.today")
        element = driver.find_element_by_link_text("News")
        element.click()
        driver.implicitly_wait(3)
        cards = self.driver.find_elements_by_class_name('col-sm-4')
        self.assertGreaterEqual(len(cards), 3)  


    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()