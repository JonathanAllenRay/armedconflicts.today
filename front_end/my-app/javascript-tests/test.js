const about = require('./about-testing.js');
const consume_api = require('./consume_api-testing.js');
const assert = require('assert');


    // script is now loaded and executed.
    // put your dependent JS here.
describe('consume_api', function() {
  //consume_api tests
  describe('get_all_conflicts', function() {
  	it('should test that the length of the response is > 0', async () => {
      const response = await consume_api.get_all_conflicts();
    	assert.equal(response.length > 0, true);
    });
  });
  describe('get_instance_by_id', function() {
  	it('should test that the length of the response is > 0', async () => {
      const response = await consume_api.get_instance_by_id(0, "conflicts");
      assert.equal(response.length > 0, true);
    });
  });
  describe('get_instance_by_name', function() {
  	it('should test that the length of the response is > 0', async () => {
      const response = await consume_api.get_instance_by_name("Afghanistan");
      assert.equal(response.length > 0, true);
    });
  });
  describe('get_cards_page', function() {
  	it('should test that the length of the response is > 0', async () => {
      const response = await consume_api.get_cards_page(0, 9, "conflicts");
      assert.equal(response.length > 0, true);
    });
  });
  describe('generic_get', function() {
  	it('should test that the length of the response is > 0', async () => {
      const response = await consume_api.generic_get("countries/name/Afghanistan");
      assert.equal(response.length > 0, true);
    });
  });
  describe('get_table_count', function() {
  	it('should test that the length of the response is > 0', async () => {
      const response = await consume_api.get_table_count("conflicts");
      assert.equal(response.length > 0, true);
    });
    it('should test that the length of the response is > 0', async () => {
      const response = await consume_api.get_table_count("countries");
      assert.equal(response.length > 0, true);
    });
    it('should test that the length of the response is > 0', async () => {
      const response = await consume_api.get_table_count("news");
      assert.equal(response.length > 0, true);
    });
  });
});

describe('about', function() {
  //about tests
  this.timeout(10000);
  describe('commits', function() {
    it('should test that duneCommits > 0', async function() {
      const commitsList = await about.commits();
      assert.equal(commitsList.duneCommits > 0, true);
    });
    it('should test that jonathanCommits > 0', async function() {
      const commitsList = await about.commits();
      assert.equal(commitsList.jonathanCommits > 0, true);
    });
    it('should test that danaCommits > 0', async function() {
      const commitsList = await about.commits();
      assert.equal(commitsList.danaCommits > 0, true);
    });
    it('should test that dylanCommits > 0', async function() {
      const commitsList = await about.commits();
      assert.equal(commitsList.dylanCommits > 0, true);
    });
    it('should test that evanCommits > 0', async function() {
      const commitsList = await about.commits();
      assert.equal(commitsList.evanCommits > 0, true);
    });
    it('should test that garretCommits > 0', async function() {
      const commitsList = await about.commits();
      assert.equal(commitsList.garrettCommits > 0, true);
    });
  });
  describe('issues', function() {
  	it('should test that duneIssues > 0', async function() {
    	const issuesList = await about.issues();
      assert.equal(issuesList.duneIssues > 0, true);
    });
    it('should test that jonathanIssues > 0', async function() {
      const issuesList = await about.issues();
      assert.equal(issuesList.jonathanIssues > 0, true);
    });
    it('should test that danaIssues == 0', async function() {
      const issuesList = await about.issues();
      assert.equal(issuesList.danaIssues == 0, true);
    });
    it('should test that dylanIssues > 0', async function() {
      const issuesList = await about.issues();
      assert.equal(issuesList.dylanIssues > 0, true);
    });
    it('should test that evanIssues == 0', async function() {
      const issuesList = await about.issues();
      assert.equal(issuesList.evanIssues == 0, true);
    });
    it('should test that garrettIssues == 0', async function() {
      const issuesList = await about.issues();
      assert.equal(issuesList.garrettIssues == 0, true);
    });
  });
});
