import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './containers/Home.js';
import DefaultNavbar from './components/DefaultNavbar.js';
import Main from './containers/Main.js'
import { Switch } from 'react-router-dom';

// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
class App extends Component {
  render() {
    return (
      <div>
        <DefaultNavbar /> 
        <Main />
      </div>
    );
  }
}

export default App;
