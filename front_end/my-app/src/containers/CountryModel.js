import React, { Component } from 'react';
import logo from '../logo.svg';
import './Home.css';
import { Link } from 'react-router-dom';
import DefaultNavbar from '../components/DefaultNavbar.js';
import RelatedContent from '../components/RelatedContent.js';
import { get_all_conflicts, get_instance_by_id, get_instance_by_name } from '../js/consume_api.js';



// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
// https://www.npmjs.com/package/react-slideshow-image
class CountryModel extends Component {

  constructor(props) {
    super(props);
    this.state = {
      description: 'No Description.',
      region: '',
      img_url: 'N/A', 
      name: this.props.match.params.countryName,
      population: 0,
      num_conflicts: 0,
      capital: '',
      maps_url: '',
      relatedNewsRef: '',
      related_conflicts: '',
    }
  }

  renderMainContent() {
  	return (
  	<div>
      <h1 class="display-4">{this.state.name}</h1>
      <hr class="hr-general" />
      <div class="div-model-basic div-extra-padding">
        <img src={this.state.img_url} class="img-rounded" alt="No image available." />
        <div>
          <p><strong>Description:</strong> {this.state.description}</p>
          <p><strong>Region:</strong> {this.state.region}</p>
          <p><strong>Population:</strong> {this.state.population}</p>
          <p><strong>Capital:</strong> {this.state.capital}</p>
          <p><strong>Number of ongoing conflicts:</strong> {this.state.num_conflicts}</p>
          <p><a href={this.state.maps_url} class="generic-link" target="_blank"><strong>Google Maps</strong></a></p>
          <p><a href={this.state.charity_url} class="generic-link" target="_blank"><strong>Charity Navigator</strong></a></p>
        </div>
      </div>
    </div>
  	);
  }

  render() {
    return (
    <div class="container black-container">
		{this.renderMainContent()}
    <RelatedContent
      modelType='news'
      modelTypeString='News'
      conflictId='1'           
      ref={instance => { 
            this.state.relatedNewsRef = instance;
          }}
    />
    <RelatedContent
      modelType='conflict'
      countryName={this.state.name}
      modelTypeString='Conflicts'
    />
    </div>

    );
  }

  mountedLogic() {
    get_instance_by_name(this.state.name).then(object => {
      try {
        this.setState({
          description: object[0]['description'],
          region: object[0]['region'],
          img_url: object[0]['flag'], 
          population: object[0]['population'],
          num_conflicts: object[0]['num_ongoing_conflicts'],
          conflicts: object[0]['related_conflicts'],
          capital: object[0]['capital'],
          related_conflicts: object[0]['related_conflicts'],
          maps_url: 'https://www.google.com/maps/place/' + object[0]['name'],
          charity_url: 'https://www.charitynavigator.org/index.cfm?keyword_list=' + this.state.name+ '&bay=search.results',
        });
        this.state.relatedNewsRef.setConflictId(object[0]['related_conflicts'][0]);
      }
      catch(error) {
        this.props.history.push('/error')
      }
    });
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    this.mountedLogic();
  }
}

export default CountryModel;
