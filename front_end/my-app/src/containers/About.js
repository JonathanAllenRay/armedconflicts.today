import React, { Component } from 'react';
import logo from '../logo.svg';
import { Switch } from 'react-router-dom';
import DemoAboutStats from '../components/DemoAboutStats.js'
import AboutCard from '../components/AboutCard.js'
import AboutInfo from '../components/AboutInfo.js';
import { commits, issues, dummy
} from '../js/about.js';
import $ from 'jquery';



// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
// https://www.npmjs.com/package/react-slideshow-image
// https://reactstrap.github.io/components/carousel/
// Card template from: https://getbootstrap.com/docs/4.0/components/card/ -->
class About extends Component {


  constructor(props) {
    super(props);
    var array = []
    this.state = {
        total_commits_display: 0,
        total_issues_display: 0,
        total_tests_display: 0, 
        callbacks: array,
    }
  }


  // If statement prevent double adding callbacks
  render() {
    return (
    <div class="container black-container">
      <h1 class="display-4">About Us</h1>
      <hr class="hr-general" />
      <div class="row justify-content-center" id="homerow">
        <AboutCard 
        ref={instance => { 
          if (this.state.callbacks.length < 6) {
            this.state.callbacks.push(instance);
            }
          }}
        devName = 'dune'
        displayName = 'Dune Blum' 
        blog="https://dblumswe.wordpress.com/"
        likes="Fashion, Travel"
        dislikes="Sock with sandals"
        role="Back-end/API"
        imgURL="https://scontent-dfw5-1.xx.fbcdn.net/v/t31.0-8/15304519_373991716275602_665981232630561659_o.jpg?_nc_cat=105&_nc_oc=AQldT6aeyNivuYMcTohYJQigk9unKqomrtIkrF2WMcUm9ps_cDiO19ARXM9l2OvFDyQ&_nc_ht=scontent-dfw5-1.xx&oh=a1f08b72041ec46ea1c77f25df1e37f4&oe=5DF1FD13"
        /> 
        <AboutCard 
        ref={instance => { 
          if (this.state.callbacks.length < 6) {
            this.state.callbacks.push(instance);
            }
          }}
        devName = 'jonathan'
        displayName = 'Jonathan Ray' 
        blog="https://jonathanray371.wordpress.com/"
        likes="Places, Things"
        dislikes="Other"
        role="Full-Stack"
        imgURL="https://jonathanray371.files.wordpress.com/2017/09/cropped-cropped-12891743_248334948845729_2327206193685984615_o11.jpg"
        />

        <AboutCard 
        ref={instance => { 
          if (this.state.callbacks.length < 6) {
            this.state.callbacks.push(instance);
            }
          }}
        devName = 'dana'
        displayName = 'Dana Vaziri' 
        blog="https://cs373fall2018seyeddanavaziri.wordpress.com/"
        likes="Politics, Soccer"
        dislikes="Sand"
        role="Testing/Back-end"
        imgURL="https://cs373fall2018seyeddanavaziri.files.wordpress.com/2018/01/prof-pic-e1516592375722.jpg?w=300&h=300"
        />
      </div>
      <div class="row justify-content-center" id="homerow">
        <AboutCard 
        ref={instance => { 
          if (this.state.callbacks.length < 6) {
            this.state.callbacks.push(instance);
            }
          }}
        devName = 'dylan'
        displayName = 'Dylan Bottoms' 
        blog="https://dylanbottoms.wordpress.com/2018/10/15/weekly-blog-post-5/"
        likes="Sports, Hip-Hop"
        dislikes="Programming"
        role="Design, Management"
        imgURL="https://dylanbottoms.files.wordpress.com/2018/09/blog.jpg"
        />

        <AboutCard 
        ref={instance => { 
          if (this.state.callbacks.length < 6) {
            this.state.callbacks.push(instance);
            }
          }}
        devName = 'evan'
        displayName = 'Evan Heinschel' 
        blog="https://evanheintschelcs373.wordpress.com/"
        likes="Beer, Film"
        dislikes="Scooters"
        role="Deployment"
        imgURL="https://evanheintschelcs373.files.wordpress.com/2018/09/headshot.png"
        />

        <AboutCard 
        ref={instance => { 
          if (this.state.callbacks.length < 6) {
            this.state.callbacks.push(instance);
            }
          }}
        devName = 'garrett'
        displayName = 'Garrett Bishop' 
        blog="https://garrettbishop888.wordpress.com/"
        likes="Sleeping, Gaming"
        dislikes="Hoverboards"
        role="Testing"
        imgURL="https://garrettbishop888.files.wordpress.com/2018/09/063-gv-2_pp-edit-e1535939488369.jpg?w=170&h=184"
        />
      </div>
      <h4>Totals (Commits/Issues/Tests): {this.state.commits_total_display}/{this.state.issues_total_display}/0</h4>
      <AboutInfo />
    </div>
    );
  } 

  componentDidMount() {
    var commits_total = 0;
    var issues_total = 0;
    var tests_total = 0;
    commits().then(commitsList => {
        for (var i = 0; i < this.state.callbacks.length; i++) { 
           this.state.callbacks[i].setCommits(commitsList);
        }
        commits_total += commitsList.duneCommits;
        commits_total += commitsList.dylanCommits;
        commits_total += commitsList.evanCommits;
        commits_total += commitsList.danaCommits;
        commits_total += commitsList.jonathanCommits;
        commits_total += commitsList.garrettCommits;
        this.setState({
          commits_total_display: commits_total,
        });
    });
    issues().then(issuesList => {
        for (var i = 0; i < this.state.callbacks.length; i++) { 
           this.state.callbacks[i].setIssues(issuesList);
        }
        issues_total += issuesList.duneIssues;
        issues_total += issuesList.dylanIssues;
        issues_total += issuesList.evanIssues;
        issues_total += issuesList.danaIssues;
        issues_total += issuesList.jonathanIssues;
        issues_total += issuesList.garrettIssues;
        this.setState({
          issues_total_display: issues_total,
        });
    });

  }
}

export default About;
