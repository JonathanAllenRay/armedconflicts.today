import React, { Component } from 'react';
import logo from '../logo.svg';
import './Home.css';
import { Link } from 'react-router-dom';
import DefaultNavbar from '../components/DefaultNavbar.js'
import Slideshow from '../components/Slideshow.js'
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';


// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
// https://www.npmjs.com/package/react-slideshow-image
class NotFound extends Component {
  render() {
    return (
      <div class="container">
        <div class="jumbotron">
          <h1 class="display-4">Sorry, page not found.</h1>
          <hr class="my-4" />
          <a class="btn btn-primary btn-lg" role="button"><Link to='/'>Return Home</Link></a>
        </div>
      </div>
    );
  }
}

export default NotFound;
