import React, { Component } from 'react';
import logo from '../logo.svg';
import { Switch, Route } from 'react-router-dom';
import Home from './Home.js';
import Search from './Search.js';
import About from './About.js';
import Conflicts from './Conflicts.js';
import Countries from './Countries.js';
import News from './News.js';
import ConflictModel from './ConflictModel.js';
import CountryModel from './CountryModel.js';
import NewsModel from './NewsModel.js';
import NotFound from './NotFound.js';

// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
// https://www.npmjs.com/package/react-slideshow-image
// https://reactstrap.github.io/components/carousel/
class Main extends Component {
  render() {
    return (
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route exact path='/About' component={About}/>
        <Route exact path='/Conflicts' component={Conflicts}/>
        <Route exact path='/Countries' component={Countries}/>
        <Route exact path='/News' component={News}/>
        <Route exact path='/Conflicts/:conflictId' component={ConflictModel}/>
        <Route exact path='/Countries/:countryName' component={CountryModel}/>
        <Route exact path='/News/:newsId' component={NewsModel}/>
        <Route exact path='/Search' component={Search}/>
        <Route component={NotFound}/>
      </Switch>
    );
  }
}



export default Main;
