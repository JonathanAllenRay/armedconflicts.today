import React, { Component } from 'react';
import logo from '../logo.svg';
import './Home.css';
import { Link } from 'react-router-dom';
import DefaultNavbar from '../components/DefaultNavbar.js';
import RelatedContent from '../components/RelatedContent.js';
import { get_all_conflicts, get_instance_by_id } from '../js/consume_api.js';

// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
// https://www.npmjs.com/package/react-slideshow-image
class NewsModel extends Component {

  constructor(props) {
    super(props);
    this.state = {
      date: 'No Description.',
      author: 0,
      source: 'N/A', 
      content: 'N/A',
      img_url: 0,
      title: 0,
      url: '',
      conflict_url: '/',
      id: this.props.match.params.newsId,
      relatedNewsRef: '',
      relatedCountriesRef: '',
    }
  }

  renderMainContent() {
  	return (
  	<div>
      <h1 class="display-4">{this.state.title}</h1>
      <hr class="hr-general" />
      <div class="div-model-basic div-extra-padding">
        <img src={this.state.img_url} class="img-rounded" alt="No image available." />
        <div>
          <p><strong>Content: </strong>{this.state.content}</p>
          <p><strong>Date:</strong> {this.state.date}</p>
          <p><strong>Source:</strong> {this.state.source}</p>
          <p><strong>Author:</strong> {this.state.author}</p>
          <Link to={this.state.conflict_url}><p><a class="generic-link"><strong>Read More: {this.state.related_conflict}</strong></a></p></Link>
          <p><a href={this.state.url} class="generic-link" target="_blank"><strong>Full Article</strong></a></p>
        </div>
      </div>
    </div>
  	);
  }

  render() {
    return (
    <div class="container black-container">
		{this.renderMainContent()}
		<RelatedContent
      modelType='news'
      modelTypeString='News'
      conflictId='1'           
      ref={instance => { 
            this.state.relatedNewsRef = instance;
          }} />
    <RelatedContent
      modelType='country'
      modelTypeString='Countries'
      conflictId='1'           
      ref={instance => { 
            this.state.relatedCountriesRef = instance;
          }} />
    </div>
    );
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if(nextProps.match.params.newsId !== prevState.id){
      return { id: nextProps.match.params.newsId }; 
    }
    else {
      return null;
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.match.params.newsId !== this.state.id){
      this.mountingLogic();
    }
  }

  mountingLogic() {
    get_instance_by_id(this.state.id, 'news').then(object => {
      try {
        this.setState({
          content: object[0]['description'],
          date: object[0]['date_str'],
          img_url: object[0]['picture'], 
          url: object[0]['url'],
          source: object[0]['source'],
          title: object[0]['title'],
          related_conflict: object[0]['conflict_name'],
          conflict_url: "/Conflicts/" + object[0]['related_conflict_id'],
          author: object[0]['author']
        });
        this.state.relatedNewsRef.setConflictId(object[0]['related_conflict_id']);
        this.state.relatedCountriesRef.setConflictId(object[0]['related_conflict_id']);
      }
      catch(error) {
        this.props.history.push('/error')
      }
    });
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    this.mountingLogic()
  }
}
export default NewsModel;
