import React, { Component } from 'react';
import logo from '../logo.svg';
import './Home.css';
import { Link } from 'react-router-dom';
import ModelCard from '../components/ModelCard.js';
import DropdownBoxConflicts from '../components/DropdownBoxConflicts.js';
import { get_all_conflicts, get_instance_by_id, get_cards_page, get_instance_by_name, get_table_count, generic_get } from '../js/consume_api.js';
import PageTurner from '../components/PageTurner.js'
import GenericInputField from '../components/GenericInputField.js'
import DefaultNavbar from '../components/DefaultNavbar.js';


// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
// https://www.npmjs.com/package/react-slideshow-image
class Conflicts extends Component {

  constructor(props) {
    super(props);
    var array = []
    this.state = {
        callbacks: array,
        debug: 1,
        model_count: 9, // Temp value
        pathString: 'conflicts?',
        sortString: '',
        countryString: '',
        searchString: '',
        searchTerms: [''],
    }
  }

  updateCards(responseObject, callbacks) {
    if (callbacks.length < 9) {
      window.alert("Not okay.")
    }
    else {
      var temp_len = responseObject.length;
      if (temp_len > 9) {
        temp_len = 9;
      }
      if (temp_len == null) {
        temp_len = 0;
      }
      for (var i = 0; i < temp_len; i++) {
        callbacks[i].setHidden('false');
        callbacks[i].updateProps(responseObject[i]);
      }
      for (var j = temp_len; j < 9; j++) {
        // If we pass null, it means not to render this card.
        // Used if we have less than 9 cards on a page.
        callbacks[j].setHidden('true');
      }
      if (temp_len <= 0) {
        return false;
      }
    }
  }

  componentDidMount() {
    generic_get(this.state.pathString + 'results=9&offset=0').then(response => {
      this.updateCards(response, this.state.callbacks);
    });
  }

  setSort(path) {
    this.setState({
      sortString: path,
    });
    generic_get(this.state.pathString + 'results=9&offset=0&' + path + this.state.searchString).then(response => {
      this.updateCards(response, this.state.callbacks);
    });
  }

  requestPath(request) {
    if (request == "sort") {
      return this.state.sortString;
    }
    else if (request == "filter") {
      return this.state.countryString;
    }
    else if (request == "search") {
      return this.state.searchString;
    }
    return this.state.sortString;
  }

  sendInput(input) {
    var tempStore = "search="+input+"&";
    // Check if input is empty
    this.setState({
      searchString: tempStore,
    });
    generic_get(this.state.pathString + 'results=9&offset=0&' + this.state.sortString + tempStore).then(response => {
      this.updateCards(response, this.state.callbacks);
    });
    var terms = input.split(",");
    for (var i = 0; i < this.state.callbacks.length; i++) {
      this.state.callbacks[i].setSearchTerms(terms);
    }
  }

  sendInputFilterCountry(input) {
    var tempStore = "country="+input+"&";
    this.setState({
      countryString: tempStore,
    });
    window.alert(this.state.pathString + 'results=9&offset=0&' + this.state.sortString + this.state.searchString + tempStore);
    generic_get(this.state.pathString + 'results=9&offset=0&' + this.state.sortString + this.state.searchString + tempStore).then(response => {
      this.updateCards(response, this.state.callbacks);
    });
  }
  
  render() {
    return (
    <div class="container black-container">
      <h1 class="display-4">Conflicts</h1>
      <hr class="hr-general" />
      <GenericInputField  
        sendInput={this.sendInput.bind(this)}
      />
      <p>Separate search terms with comma and no space (case sensitive).</p>
      <DropdownBoxConflicts 
        clickFunction={this.setSort.bind(this)}
      />
      <div class="row justify-content-center" id="homerow">
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          searchTerms={['']}
          modelType='conflict' 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/Conflicts/1' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          searchTerms={['']}
          modelType='conflict' 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/Conflicts/1' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          searchTerms={['']}
          modelType='conflict' 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/Conflicts/1' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          searchTerms={['']}
          modelType='conflict' 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/Conflicts/1' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          searchTerms={['']}
          modelType='conflict' 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/Conflicts/1' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          searchTerms={['']}
          modelType='conflict' 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/Conflicts/1' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          searchTerms={['']}
          modelType='conflict' 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/Conflicts/1' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          searchTerms={['']}
          modelType='conflict' 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/Conflicts/1' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          searchTerms={['']}
          modelType='conflict' 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/Conflicts/1' />
      </div>
      <PageTurner 
        updater={this.updateCards.bind(this)}
        callbacks={this.state.callbacks}
        modelType='conflicts'
        pathString={this.state.pathString}
        requestPath={this.requestPath.bind(this)}
      />
    </div>
    );
  }
}

      /*
      <GenericInputField  
        sendInput={this.sendInputFilterCountry.bind(this)}
      />
      <p>Type country to filter by.</p>
      */


export default Conflicts;
