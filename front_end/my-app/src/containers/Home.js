import React, { Component } from 'react';
import logo from '../logo.svg';
import './Home.css';
import { Link } from 'react-router-dom';
import DefaultNavbar from '../components/DefaultNavbar.js'
import Slideshow from '../components/Slideshow.js'
import DropdownBox from '../components/DropdownBox.js'
import GenericInputField from '../components/GenericInputField.js'
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';


// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
// https://www.npmjs.com/package/react-slideshow-image
class Home extends Component {
  render() {
    return (
        <div class="container black-container home-container">
          <h1 class="display-4">ArmedConflicts.Today: Home</h1>
          <p class="lead">Unfortunately, there is fighting all around the world. Browse our database of conflicts, countries, news links, to learn more.</p>
          <hr class="hr-general" />
          <div class="row">
            <div class="col-sm">
              <a class="btn btn-primary btn-lg btn-home-orwidth" role="button"><Link to='/Conflicts'>Conflicts</Link></a>
            </div>
            <div class="col-sm">
              <a class="btn btn-primary btn-lg btn-home-orwidth" role="button"><Link to='/Countries'>Countries</Link></a>
            </div>
            <div class="col-sm">
              <a class="btn btn-primary btn-lg btn-home-orwidth" role="button"><Link to='/News'>News</Link></a>
            </div>
          </div>
          <Slideshow />
        </div>
    );
  }
}

export default Home;
