import React, { Component } from 'react';
import logo from '../logo.svg';
import './Home.css';
import { Link } from 'react-router-dom';
import ModelCard from '../components/ModelCard.js';
import DropdownBoxConflicts from '../components/DropdownBoxConflicts.js';
import { get_all_conflicts, get_instance_by_id, get_cards_page, get_instance_by_name, get_table_count, generic_get } from '../js/consume_api.js';
import PageTurner from '../components/PageTurner.js'
import DefaultNavbar from '../components/DefaultNavbar.js';
import Conflicts from '../containers/Conflicts.js';
import Countries from '../containers/Countries.js';
import GenericInputField from '../components/GenericInputField.js'
import News from '../containers/News.js';



// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
// https://www.npmjs.com/package/react-slideshow-image
class Search extends Component {

  constructor(props) {
    super(props);
    var array = [];
    this.state = {
        showPages: true,
        callbacks: array,
    }
  }

  sendInput(input) {
    if (this.state.showPages == false) {
      this.setState({
        showPages: true,
      });
    }
    for (var i = 0; i < this.state.callbacks.length; i++) {
      this.state.callbacks[i].sendInput(input);
    }
  }
  
  render() {
    return (
    <div>
      <div class="container black-container">
      <h1 class="display-4">Sitewide Search</h1>
      <p>This page combines the Conflicts, Countries, and News pages.</p>
       <GenericInputField 
        sendInput={this.sendInput.bind(this)}
       />
        <p>Separate search terms with comma and no space (case sensitive).</p>
      <hr class="hr-general" />       
      </div>
      {this.renderPages()}
    </div>
    );
  }

  // This function is separated out because
  // we intended to only show the pages once a search
  // has been called. Not implemented yet due to troubles
  // with highlighting.
  renderPages() {
    if (this.state.showPages == true) {
      return (
      <div>
      <Conflicts 
        ref={instance => { 
          if (this.state.callbacks.length < 3) {
            this.state.callbacks.push(instance);
          }
        }}
      />
      <Countries
        ref={instance => { 
          if (this.state.callbacks.length < 3) {
            this.state.callbacks.push(instance);
          }
        }}
      />
      <News
        ref={instance => { 
          if (this.state.callbacks.length < 3) {
            this.state.callbacks.push(instance);
          }
        }}
      />
      </div>
    );
    }
  }
}



export default Search;
