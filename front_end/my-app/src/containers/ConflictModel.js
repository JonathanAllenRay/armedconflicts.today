import React, { Component } from 'react';
import logo from '../logo.svg';
import './Home.css';
import { Link } from 'react-router-dom';
import DefaultNavbar from '../components/DefaultNavbar.js';
import RelatedContent from '../components/RelatedContent.js';
import { get_all_conflicts, get_instance_by_id } from '../js/consume_api.js';



// Navbar borrowed from: https://m.pardel.net/react-and-bootstrap-4-part-1-setup-navigation-d4767e2ed9f0
// https://www.npmjs.com/package/react-slideshow-image
class ConflictModel extends Component {

  constructor(props) {
    super(props);
    this.state = {
      description: 'No Description.',
      year_started: 0,
      img_url: 'N/A', 
      name: 'N/A',
      deaths: 0,
      recent_deaths: 0,
      url: '',
      id: this.props.match.params.conflictId,
    }
  }

  renderMainContent() {
  	return (
  	<div>
      <h1 class="display-4">{this.state.name}</h1>
      <hr class="hr-general" />
      <div class="div-model-basic div-extra-padding">
        <img src={this.state.img_url} class="img-rounded" alt="No image available." />
        <div>
          <p><strong>Description: </strong>{this.state.description}</p>
          <p><strong>Year Started: </strong>{this.state.year_started}</p>
          <p><strong>Deaths: </strong>{this.state.deaths}</p>
          <p><strong>Recent Deaths: </strong>{this.state.recent_deaths}</p>
          <p><a href={this.state.url} class="generic-link" target="_blank"><strong>More Info</strong></a></p>
        </div>
      </div>
    </div>
  	);
  }

  render() {
    return (
    <div class="container black-container">
		{this.renderMainContent()}
    <RelatedContent
      modelType='news'
      modelTypeString='News'
      conflictId={this.state.id} />
    <RelatedContent
      modelType='country'
      modelTypeString='Countries'
      conflictId={this.state.id}  />
    </div>
    );
  }

  componentDidMount() {
    get_instance_by_id(this.state.id, 'conflicts').then(object => {
      try {
        this.setState({
          name: object[0]['name'],
          description: object[0]['description'],
          img_url: object[0]['picture'], 
          deaths: object[0]['deaths'],
          recent_deaths: object[0]['recent_deaths'],
          year_started: object[0]['year_started'],
          url: 'https://en.wikipedia.org/wiki/' + object[0]['name'],
        });
      }
      catch(error) {
        this.props.history.push('/error')
      }
    });
    window.scrollTo(0, 0);
  }
}

export default ConflictModel;
