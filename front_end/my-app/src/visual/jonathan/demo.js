function demolol() {
	window.alert("Demolol");
}

// Not ideal for clarity sake, but good for testing or temp use
async function generic_get(path) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/' + path, 
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
}

// This is not actually needed anymore, however, I am
// keeping it here for reference in case I need to
// remember how to use await in the future.
async function get_data_object() {
  var conflict_names = await generic_get("conflicts");
  var result = [];
  for (var i = 0; i < conflict_names.length; i++) {
  	  var temp_name = conflict_names[i]['name'];
	  var news = await generic_get("news?search=" + temp_name);
	  var obj = {
	  	name: temp_name,
	  	count: news.length,
	  }
	  result.push(obj);
  }
  return result;
}

async function get_year_conflict_data() {
  var result = [];
  var conflicts = await generic_get("conflicts?sort=year_started");
  for (var i = 0; i < conflicts.length; i++) {
	  var obj = {
	  	name: conflicts[i]['name'],
	  	year_started: conflicts[i]['year_started'],
	  }
	  result.push(obj);
  }
  return result;
}