var jsdom = require('jsdom');
const { JSDOM } = jsdom;
const { window } = new JSDOM();
const { document } = (new JSDOM('')).window;
global.document = document;

var $ = jQuery = require('jquery')(window);


module.exports = {
get_all_conflicts : async function get_all_conflicts() {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/conflicts',
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
},

get_instance_by_id: async function get_instance_by_id(id, type) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/' + type + '/' + id,
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
},

// Only allowed for country
get_instance_by_name : async function get_instance_by_name(name) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/countries/name/' + name,
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
},

// Uses the API to get a range of JSON objects
// Usually 9 at a time or less.
// Note: Parameters not used yet
// Should be re-named, probably for clarity
get_cards_page : async function get_cards_page(start, limit, type) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/' + type +'?offset=' + start + '&results=' + limit, 
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
},

// Not ideal for clarity sake, but good for testing or temp use
async function generic_get(path) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/' + path, 
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
},

// Note: Parameters not used yet
get_table_count : async function get_table_count(table_name) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/count/' + table_name, 
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
}
}

function demo() {
  window.alert("Demo function working");
}

// AWS url http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/count/