import React, { Component } from 'react';
import $ from 'jquery';
import { commits, issues, dummy
} from '../js/about.js'

import '../global_css/web.css';

class AboutCard extends Component {

  // Note this is called before didMount()
  constructor(props) {
    super(props);
    this.state = {  commits: 0,
                    issues: 0,
                    name: this.props.devName,
                    displayName: this.props.displayName,
                    blogURL: this.props.blog,
                    likes: this.props.likes,
                    dislikes: this.props.dislikes,
                    role: this.props.role,
                    imgURL: this.props.imgURL };
  }

  render() {
    return (
	  <div class="col-sm-4">
	    <div class="card home-card">
	      <img class="card-img-top" src={this.state.imgURL} alt="Card image cap" />
	      <div class="card-body">
	        <h5 class="card-title">{this.state.displayName}</h5>
	        <p class="card-text">Role: {this.state.role}</p>
	        <p class="card-text">Likes: {this.state.likes}</p>
	        <p class="card-text">Dislikes: {this.state.dislikes}</p>
	        <p class="card-text">Commits: {this.state.commits}</p>
	        <p class="card-text">Issues: {this.state.issues}</p>
	        <p class="card-text">Unit Tests: </p>
	        <a href={this.state.blogURL} class="btn btn-primary home-card-button" target='_blank'>Blog</a>
	      </div>
	    </div>
	  </div>
    );
  }

  setCommits(commitsList) {
    switch(this.state.name) {
      case 'dune':
        this.setState({
          commits: commitsList.duneCommits
        });
        break;
      case 'jonathan':
        this.setState({
          commits: commitsList.jonathanCommits
        });
        break;
      case 'dylan':
        this.setState({
          commits: commitsList.dylanCommits
        });
        break;
      case 'evan':
        this.setState({
          commits: commitsList.evanCommits
        });
        break;
      case 'garrett':
        this.setState({
          commits: commitsList.garrettCommits
        });
        break;
      case 'dana':
        this.setState({
          commits: commitsList.danaCommits
        });
        break;
      default:
        break
      }
  }

  setIssues(issuesList) {
    switch(this.state.name) {
      case 'dune':
        this.setState({
          issues: issuesList.duneIssues
        });
        break;
      case 'jonathan':
        this.setState({
          issues: issuesList.jonathanIssues
        });
        break;
      case 'dylan':
        this.setState({
          issues: issuesList.dylanIssues
        });
        break;
      case 'evan':
        this.setState({
          issues: issuesList.evanIssues
        });
        break;
      case 'garrett':
        this.setState({
          issues: issuesList.garrettIssues
        });
        break;
      case 'dana':
        this.setState({
          issues: issuesList.danaIssues
        });
        break;
      default:
        break
      }
  }
}

export default AboutCard;