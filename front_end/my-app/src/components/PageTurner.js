import React, { Component } from 'react';
import $ from 'jquery';
import { commits, issues, dummy
} from '../js/about.js'
import { get_all_conflicts, get_instance_by_id, get_cards_page, get_table_count, generic_get} from '../js/consume_api.js';
import '../global_css/web.css';
import { Button } from 'reactstrap';


class PageTurner extends Component {

  // Note this is called before didMount()
  constructor(props) {
    super(props);
    this.state = {
      modelType: this.props.modelType,
      current_page: 1,
      first_page: 1,
      max_page: 5, //Temp val
      update_function: this.props.updater,
      callbacks: this.props.callbacks,
      pathString: this.props.pathString,
      requestPath: this.props.requestPath,
    }
  }

  back() {
    if (this.state.current_page > this.state.first_page) {
      this.setState({
        current_page: this.state.current_page - 1,
      });
      var offset = (this.state.current_page-2)*9;
      var finalPath = this.state.pathString + 'offset=' + offset + '&results=9&';
      finalPath += this.state.requestPath("sort");
      finalPath += this.state.requestPath("search");
      finalPath += this.state.requestPath("filter");
      generic_get(finalPath).then(response => {
        this.state.update_function(response, this.state.callbacks);
      });
    }
  }

  next() {
    if (this.state.current_page < this.state.max_page) {
      this.setState({
        current_page: this.state.current_page + 1,
      });
      var offset = (this.state.current_page)*9;
      var finalPath = this.state.pathString + 'offset=' + offset + '&results=9&';
      finalPath += this.state.requestPath("sort");
      finalPath += this.state.requestPath("search");
      finalPath += this.state.requestPath("filter");
      generic_get(finalPath).then(response => {
        this.state.update_function(response, this.state.callbacks);
      });
    }
  }

  first() {
    if (this.state.current_page > this.state.first_page) {
      this.setState({
        current_page: this.state.first_page,
      });
      var offset = 0;
      var finalPath = this.state.pathString + 'offset=' + offset + '&results=9&';
      finalPath += this.state.requestPath("sort");
      finalPath += this.state.requestPath("search");
      finalPath += this.state.requestPath("filter");
      generic_get(finalPath).then(response => {
        this.state.update_function(response, this.state.callbacks);
      });
    }

  }

  last() {
    if (this.state.current_page < this.state.max_page) {
      this.setState({
        current_page: this.state.max_page,
      });
      var offset = (this.state.max_page-1)*9;
      var finalPath = this.state.pathString + 'offset=' + offset + '&results=9&';
      finalPath += this.state.requestPath("sort");
      finalPath += this.state.requestPath("search");
      finalPath += this.state.requestPath("filter");
      generic_get(finalPath).then(response => {
        this.state.update_function(response, this.state.callbacks);
      });
    }
  }

  componentDidMount() {
    get_table_count(this.state.modelType).then(response => {
      var pages = parseInt(response[0]['count'] / 9, 0);
      var mod = response[0]['count'] % 9;
      if (mod > 0) {
        pages += 1;
      } 
      this.setState({
        max_page: pages,
      });
    });
  }


  render() {
    return (
      <div class="container">
        <div class="row">
          <div class="col-sm">
            <a class="btn btn-primary btn-sm btn-pg-color" role="button" onClick={(e) => this.first(e)}>&lt;&lt;&lt; First</a>
          </div>
          <div class="col-sm">
            <a class="btn btn-primary btn-sm btn-pg-color" role="button" onClick={(e) => this.back(e)}>&lt; Back</a>
          </div>
          <div class="col-sm">
            <h1>{this.state.current_page}</h1>
          </div>
          <div class="col-sm">
            <a class="btn btn-primary btn-sm btn-pg-color" role="button" onClick={(e) => this.next(e)}>Next &gt;</a>
          </div>
          <div class="col-sm">
            <a class="btn btn-primary btn-sm btn-pg-color" role="button" onClick={(e) => this.last(e)}>Last &gt;&gt;&gt;</a>
          </div>
        </div>
      </div>
    );
  }
}

export default PageTurner;