import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import React, { Component } from 'react';

// Code borrowed from: https://reactstrap.github.io/components/dropdowns/
class DropdownBoxNews extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      title: this.props.title,
      dropdownOpen: false,
      clickFunction: this.props.clickFunction.bind(this),
    };

  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  demo_alert() {
    window.alert("Demo alert");
  }

  render() {
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} color="primary" size="lg">
        <DropdownToggle caret>
          Sort News
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={ () => this.state.clickFunction("")}>Default</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=title&")}>Title (Asc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=title&order=desc&")}>Title (Desc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=source&")}>Source (Asc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=source&order=desc&")}>Source (Desc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=conflict_name&")}>Conflict Name (Asc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=conflict_name&order=desc&")}>Conflict Name (Desc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=date_str&")}>Date (Asc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=date_str&order=desc&")}>Date (Desc.)</DropdownItem>
        </DropdownMenu>
      </Dropdown>
    );
  }
}


export default DropdownBoxNews;



