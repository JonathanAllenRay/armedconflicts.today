import React, { Component } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';
import ModelCard from './ModelCard.js';
import { commits, issues, dummy
} from '../js/about.js';
import { generic_get
} from '../js/consume_api.js'


import '../global_css/web.css';

class RelatedContent extends Component {

  // This will store the instance id or name in the
  // component state, allowing it to then call the
  // API by ID or name

  // Will need to move this to DidMount() after we
  // implement the API calls.
  constructor(props) {
    super(props);
    var array = []
    this.state = {conflictId: this.props.conflictId,
            modelType: this.props.modelType,
            modelTypeString: this.props.modelTypeString,
            limit: 3,
            callbacks: array,
            countryName: this.props.countryName,

             }
  }



  updateCards(responseObject, callbacks) {
    if (callbacks.length < 9) {
      //window.alert("Not okay.")
    }
    else {
      var temp_len = responseObject.length;
      if (temp_len > this.state.limit) {
        temp_len = this.state.limit;
      }
      if (temp_len == null) {
        temp_len = 0;
      }
      for (var i = 0; i < temp_len; i++) {
        callbacks[i].setHidden('false');
        callbacks[i].updateProps(responseObject[i]);
      }
      for (var j = temp_len; j < 9; j++) {
        // If we pass null, it means not to render this card.
        // Used if we have less than 9 cards on a page.
        callbacks[j].setHidden('true');
      }
    }
  }

  componentDidMount() {
    this.mountedLogic();
  }

  mountedLogic() {
    if (this.state.modelType == 'news') {
      generic_get("news/conflict/" + this.state.conflictId + "?offset=0&results=" + this.state.limit).then(response => {
        this.updateCards(response, this.state.callbacks);
      });
    }
    else if (this.state.modelType == 'conflict') {
      generic_get("conflicts/country/" + this.state.countryName + "?offset=0&results=" + this.state.limit).then(response => {
        this.updateCards(response, this.state.callbacks);
      });
    }
    else if (this.state.modelType == 'country') {
      generic_get("countries/conflict/" + this.state.conflictId + "?offset=0&results=" + this.state.limit).then(response => {
        this.updateCards(response, this.state.callbacks);
      });
    }
  }

  setConflictId(num) {
    this.setState({
      conflictId: num,
    });
    this.mountedLogic();
  }

  // Card template from: https://getbootstrap.com/docs/4.0/components/card/ 
  // Inside the div, 
  render() {
    return (
      <div>
        <h1 class="display-4">Related {this.state.modelTypeString}</h1>
        <hr class="hr-general" />
        <div class="row justify-content-center" id="homerow">
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          modelType={this.state.modelType} 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          modelType={this.state.modelType} 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          modelType={this.state.modelType} 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          modelType={this.state.modelType} 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          modelType={this.state.modelType} 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          modelType={this.state.modelType} 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          modelType={this.state.modelType} 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          modelType={this.state.modelType} 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/' />
        <ModelCard 
          ref={instance => { 
            if (this.state.callbacks.length < 9) {
              this.state.callbacks.push(instance);
            }
          }}
          modelType={this.state.modelType} 
          img="https://i.imgur.com/s1r4nqu.jpg" 
          url='/' />
        </div>
      </div>
    );
  }
}

export default RelatedContent;