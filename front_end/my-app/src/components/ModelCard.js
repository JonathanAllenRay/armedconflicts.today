import React, { Component } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';
import { commits, issues, dummy
} from '../js/about.js'
import { generic_get } from '../js/consume_api.js';
import '../global_css/web.css';
import Highlighter from "react-highlight-words";

class ModelCard extends Component {

  // This will store the instance id or name in the
  // component state, allowing it to then call the
  // API by ID or name

  // Will need to move this to DidMount() after we
  // implement the API calls.
  constructor(props) {
    super(props);
    var dummyval = dummy(0);
    var modelType = this.props.modelType;
    this.setSearchTerms = this.setSearchTerms.bind(this);
    if (modelType == 'conflict') {
      this.state = { modelType: modelType,
                     url: this.props.url,
                     img: this.props.img,
                     relatedCountries: this.props.relatedCountries,
                     recentDeaths: this.props.recentDeaths,
                     started: this.props.started,
                     hidden: this.props.hidden,
                     searchTerms: (this.props.searchTerms == undefined) ? [''] : this.props.searchTerms,
                    };
    }
    else if (modelType == 'country') {
      this.state = { modelType: modelType,
                     url: this.props.url,
                     title: 'N/A',
                     img: this.props.img,
                     region: this.props.region,
                     language: this.props.region,
                     conflicts: this.props.conflicts,
                     hidden: this.props.hidden,
                     searchTerms: (this.props.searchTerms == undefined) ? [''] : this.props.searchTerms,
                    };

    }
    else if (modelType == 'news') {
      this.state = { modelType: modelType,
                     url: this.props.url,
                     newsId: this.props.newsId,
                     img: this.props.img,
                     date: this.props.date,
                     source: this.props.source,
                     authors: this.props.authors,
                     hidden: this.props.hidden,
                     searchTerms: (this.props.searchTerms == undefined) ? [''] : this.props.searchTerms,
                    };
    }
    else {
      throw "No model type specified, idiot.";
    }
  }

  renderConflict() {
    if (this.state.modelType == 'conflict') {
      return (
      <div>
          <p class="card-text">Countries: {this.state.related_countries}</p>
          <p class="card-text">Deaths: {this.state.deaths}</p>
          <p class="card-text">Recent Deaths: {this.state.recent_deaths}</p>
          <p class="card-text">Started: {this.state.year_started}</p>
      <Link to={this.state.url}><a class="btn btn-primary home-card-button">Learn more</a></Link>
      </div>
    );
    }
    return null;
  }

  renderCountry() {
    if (this.state.modelType == 'country') {
      return (
      <div>
          <p class="card-text">Region: {this.state.region}</p>
          <p class="card-text">Population: {this.state.population}</p>
          <p class="card-text">Capital: {this.state.capital}</p>
          <p class="card-text">Conflicts: {this.state.conflicts}</p>
      <Link to={this.state.url}><a class="btn btn-primary home-card-button">Learn more</a></Link>
      </div>
    );
    }
    return null;
  }

  renderNews() {
    if (this.state.modelType == 'news') {
      return (
      <div>
          <p class="card-text">{this.state.title_small}</p>
          <p class="card-text">Date: {this.state.date}</p>
          <p class="card-text">Source: {this.state.source}</p>
          <p class="card-text">Author(s): {this.state.author}</p>
          <p class="card-text">Conflict: {this.state.related_conflict}</p>
      <Link to={this.state.url}><a class="btn btn-primary home-card-button">Learn more</a></Link>
      </div>
    );
    }
    return null;
  }

  setHidden(hidden) {
      this.setState({
    hidden: hidden,
      });
  }

  setSearchTerms(terms) {
    this.setState({
      searchTerms: terms,
    });
  }

  updateProps(jsonObject) {
    if (this.state.modelType == 'conflict') {
        this.setState({
           url: /Conflicts/ + jsonObject['id'],
           related_countries: jsonObject['related_countries'].join(", "),
           recent_deaths: jsonObject['recent_deaths'],
           deaths: jsonObject['deaths'],
           year_started: jsonObject['year_started'],
           title: jsonObject['name'],
           img: jsonObject['picture']
        });
    }
    else if (this.state.modelType == 'country') {
      this.setState({
       url: /Countries/ + jsonObject['name'],
       title: jsonObject['name'],
       capital: jsonObject['capital'],
       img: jsonObject['flag'],
       region: jsonObject['region'],
       population: jsonObject['population'],
       conflicts: jsonObject['num_ongoing_conflicts'],
      });
    }
    else if (this.state.modelType == 'news') {
      this.setState({
       url: /News/ + jsonObject['id'],
       title: jsonObject['title'],
       title_small: jsonObject['title'],
       author: jsonObject['author'],
       img: jsonObject['picture'],
       related_conflict: jsonObject['conflict_name'],
       date: jsonObject['date_str'],
       source: jsonObject['source']
      });
    }
    else {
      throw "No model type specified, idiot.";
    }
  }

  render() {
    if (this.state.hidden == 'true') {
      return null;
    }
    else {
      return (
      <div class="col-sm-4">
  
        <div class="card home-card">
          <Link class="card-img-top" to={this.state.url}><img class="card-img-top" src={this.state.img} alt="No img available." /></Link>
          <div class="card-body">
            <h5 class="card-title">
            <Highlighter
                highlightClassName="highlight"
                searchWords={this.state.searchTerms}
                autoEscape={true}
                textToHighlight={this.state.title}
            />
            </h5>
              {this.renderConflict()}
              {this.renderCountry()}
              {this.renderNews()}
          </div>
        </div>
      </div>
      );
    }

  }
}

export default ModelCard;
