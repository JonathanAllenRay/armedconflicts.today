import React, { Component } from 'react';
import { Link } from 'react-router-dom';


// Navbar HTML modified fromkey: view-source:https://getbootstrap.com/docs/4.1/examples/navbars/



class DefaultNavbar extends Component {
  render() {
    return (
    <nav class="navbar navbar-expand navbar-dark bg-dark">
      <a class="navbar-brand dnv-text">Armed Conflicts.Today</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarsExample02">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item dnv-text">
            <Link to='/'><a class="nav-link dnv-text">Home <span class="sr-only"></span></a></Link>
          </li>
          <li class="nav-item">
            <Link to='/About'><a class="nav-link dnv-text">About</a></Link>
          </li>
          <li class="nav-item">
            <Link to='/Conflicts'><a class="nav-link dnv-text">Conflicts</a></Link>
          </li>
          <li class="nav-item">
            <Link to='/Countries'><a class="nav-link dnv-text">Countries</a></Link>
          </li>
          <li class="nav-item">
            <Link to='/News'><a class="nav-link dnv-text">News</a></Link>
          </li>
          <li class="nav-item">
            <Link to='/Search'><a class="nav-link dnv-text">Search</a></Link>
          </li>
        </ul>
      </div>
    </nav>
    );
  }
}

/*
<form class="form-inline my-2 my-md-0">
          <input class="form-control" type="text" placeholder="Search: (Not done)" />
        </form>*/

export default DefaultNavbar;
