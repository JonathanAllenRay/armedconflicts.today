import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import React, { Component } from 'react';

// Code borrowed from: https://reactstrap.github.io/components/dropdowns/
class DropdownBoxCountries extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      title: this.props.title,
      dropdownOpen: false,
      clickFunction: this.props.clickFunction.bind(this),
    };

  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  demo_alert() {
    window.alert("Demo alert");
  }

  render() {
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} color="primary" size="lg">
        <DropdownToggle caret>
          Sort Countries
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={ () => this.state.clickFunction("")}>Default</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=name&")}>Alphabetical (Asc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=name&order=desc&")}>Alphabetical (Desc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=region&")}>Region (Asc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=region&order=desc&")}>Region (Desc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=num_ongoing_conflicts&")}>Num. Conflicts (Asc.)</DropdownItem>
          <DropdownItem onClick={ () => this.state.clickFunction("sort=num_ongoing_conflicts&order=desc&")}>Num. Conflicts (Desc.)</DropdownItem>
        </DropdownMenu>
      </Dropdown>
    );
  }
}

export default DropdownBoxCountries;



