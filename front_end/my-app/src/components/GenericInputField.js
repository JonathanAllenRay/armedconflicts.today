import React, { Component } from 'react';
import $ from 'jquery';
import { commits, issues, dummy
} from '../js/about.js'
import { get_all_conflicts, get_instance_by_id, get_cards_page, get_table_count } from '../js/consume_api.js';
import '../global_css/web.css';
import { Button } from 'reactstrap';

// Code from official docs: https://reactjs.org/docs/forms.html
// https://getbootstrap.com/docs/4.0/components/forms/
class GenericInputField extends Component {
  constructor(props) {
    super(props);
    this.state = {value: '',
                  sendInput: this.props.sendInput,
                  fieldClass: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.state.sendInput(this.state.value);
  }

  render() {
    return (
    	<div class="centered-form"> 
			<form onSubmit={this.handleSubmit}>
			  <div class="form-group mx-sm-3 mb-2">
			    <label for="field" class="sr-only">Type here...</label>
			    <input type="text" class="form-control search-width" id="field" placeholder="Type here..." value={this.state.value} onChange={this.handleChange} />
			  </div>
			  <button type="submit" class="btn btn-primary btn-sm btn-home-orwidth">Submit</button>
			</form>
			</div>
    );
  }
}

export default GenericInputField;