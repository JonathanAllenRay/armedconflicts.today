import React, { Component } from 'react';

class AboutInfo extends Component {


  render() {
    return (
      <div>
        <hr class="hr-general" />
        <h1 class="display-4">ArmedConflicts.Today: About The Website</h1>
        <hr class="hr-general" />
        <h2 class="display-4">Website Info</h2>
        <p>The purpose of ArmedConflicts.Today is to help inform the public about violent conflicts around the world. There are
          many wars and conflicts that most people do not know about. This website provides information about the conflicts, related 
          countries, and relevant news.</p>
        <h2 class="display-4">Data Sources</h2>
        <div class="tools-div-box">
          <ul class="list-group about-list">
            <li class="list-group-item">https://en.wikipedia.org/wiki/List_of_ongoing_armed_conflicts: List of conflicts and country information.</li>
            <li class="list-group-item">https://restcountries.eu/: Additional info about countries.</li>
            <li class="list-group-item">https://newsapi.org/: Related news.</li>
            <li class="list-group-item">https://www.charitynavigator.org/: Related charities.</li>
          </ul>
        </div>
        <h2 class="display-4">Software Development Tools</h2>
        <div class="tools-div-box">
          <ul class="list-group about-list">
            <li class="list-group-item">HTML5, CSS3: Basic building blocks of web development.</li>
            <li class="list-group-item">Bootstrap: Used for styling pages and creating grids of cards.</li>
            <li class="list-group-item">Git, GitLab, Sublime Text, Slack, NPM: Tools to aid with creating the software.</li>
            <li class="list-group-item">Amazon Web Services S3: Hosting for the static website.</li>
            <li class="list-group-item">Amazon Web Services RDS/EC2: Hosting for the dynamic website.</li>
            <li class="list-group-item">Docker: Containerization for testing, deployment, and developing.</li>
            <li class="list-group-item">NPM/PIP: Managing dependencies.</li>
            <li class="list-group-item">NameCheap: Domain purchasing.</li>
            <li class="list-group-item">Postman: Designing and documenting the RESTful API.</li>
            <li class="list-group-item">React/React-Router: Framework for front-end.</li>
            <li class="list-group-item">Reactstrap: Used to create carousel slideshow.</li>
            <li class="list-group-item">JQuery: Getting About page statistics.</li>
            <li class="list-group-item">PostgreSQL: Database for storing our data. Hosted on AWS RDS.</li>
            <li class="list-group-item">PGAdmin: Viewing and managing database.</li>
            <li class="list-group-item">PsycoPG2: Python module used to add and edit data in the database.</li>
            <li class="list-group-item">Wikipedia Module: Python module used to fetch descriptions from Wikipedia as a string.</li>
            <li class="list-group-item">https://github.com/rocheio/wiki-table-scrape: Used to scrape Wikipedia tables into CSV format.</li>
            <li class="list-group-item">https://www.csvjson.com/csv2json: Used to convert Wikipedia table CSV to JSON which was then put into our database.</li>
            <li class="list-group-item">Flask CORS: Used for testing on the same machine locally.</li>
          </ul>
        </div>
        <h2 class="display-4">Attribution</h2>
        <div class="tools-div-box">
          <ul class="list-group about-list">
            <li class="list-group-item">N/A</li>
          </ul>
        </div>
        <h2 class="display-4">Other Links</h2>
        <div class="tools-div-box">
          <ul class="list-group about-list">
            <li class="list-group-item">Repo: https://gitlab.com/JonathanAllenRay/armedconflicts.today</li>
            <li class="list-group-item">Postman API: https://documenter.getpostman.com/view/5473416/RWgjaMgk</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default AboutInfo;
