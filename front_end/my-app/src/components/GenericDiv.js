import React, { Component } from 'react';
import $ from 'jquery';
import { commits, issues, dummy
} from '../js/about.js'

class DemoAboutStats extends Component {

  // Note this is called before didMount()
  constructor(props) {
    super(props);
    var dummyval = dummy(0);
    this.state = { commits: dummyval,
                    issues: 0,
                    name: this.props.devName };
  }

  componentDidMount() {
    var dummy = 0;
    commits().then(commitsList => {
        this.setState((state, props) => {
          switch(this.state.name) {
              case 'dune':
                return {commits: commitsList.duneCommits};
                break;
              case 'jonathan':
                return {commits: commitsList.jonathanCommits};
                break;
              case 'dylan':
                return {commits: commitsList.dylanCommits};
                break;
              case 'evan':
                return {commits: commitsList.evanCommits};
                break;
              case 'garrett':
                return {commits: commitsList.garrettCommits};
                break;
              case 'dana':
                return {commits: commitsList.danaCommits};
                break;
              default:
                break
          }
        });
    });
    issues().then(issuesList => {
        this.setState((state, props) => {
          switch(this.state.name) {
              case 'dune':
                return {issues: issuesList.duneIssues};
                break;
              case 'jonathan':
                return {issues: issuesList.jonathanIssues};
                break;
              case 'dylan':
                return {issues: issuesList.dylanIssues};
                break;
              case 'evan':
                return {issues: issuesList.evanIssues};
                break;
              case 'garrett':
                return {issues: issuesList.garrettIssues};
                break;
              case 'dana':
                return {issues: issuesList.danaIssues};
                break;
              default:
                break
          }
        });
    });

  }

  render() {
    return (
        <div>
            <h1>{this.state.commits}</h1>
            <h1>{this.state.issues}</h1>
            <h1>{this.state.name}</h1>
        </div>
    );
  }
}

export default DemoAboutStats;