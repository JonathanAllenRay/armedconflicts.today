import $ from 'jquery';



export async function get_all_conflicts() {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-3-15-171-61.us-east-2.compute.amazonaws.com/conflicts',
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
}

export async function get_instance_by_id(id, type) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-3-15-171-61.us-east-2.compute.amazonaws.com/' + type + '/' + id,
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
}

// Only allowed for country
export async function get_instance_by_name(name) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-3-15-171-61.us-east-2.compute.amazonaws.com/countries/name/' + name,
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
}

// Uses the API to get a range of JSON objects
// Usually 9 at a time or less.
// Note: Parameters not used yet
// Should be re-named, probably for clarity
export async function get_cards_page(start, limit, type) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-3-15-171-61.us-east-2.compute.amazonaws.com/' + type +'?offset=' + start + '&results=' + limit, 
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
}

// Not ideal for clarity sake, but good for testing or temp use
export async function generic_get(path) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-3-15-171-61.us-east-2.compute.amazonaws.com/' + path, 
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
}

// Note: Parameters not used yet
export async function get_table_count(table_name) {
  var settings = {
    "async": false,
    "crossDomain": true,
    "url": 'http://ec2-3-15-171-61.us-east-2.compute.amazonaws.com/count/' + table_name, 
    "method": "GET",
    "timeout": 3000, 
  }
  var response_result = null;
  try {
    await $.ajax(settings).done(function (response) {
      response_result = response;
    });
  }
  catch (error) {
    console.error(error);
  }
  return response_result;
}

// AWS url http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/count/