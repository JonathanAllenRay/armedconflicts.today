import json
import psycopg2
import requests
import math
import wikipedia

curr_conflict_id = 0
curr_country_id = 0
###
# Test functions, not core part of module, but useful for reference
###
def get_countries_from_json_demo() : 
    with open('countries.json', errors='ignore') as json_data:    
        data = json.load(json_data)
        for country in data:
            print(country.get("flag"))

def get_countries_from_url_demo() :
    response = requests.get('https://restcountries.eu/rest/v2/all')
    data = response.json()
    for country in data:
        print(country.get("flag"))

def get_news_from_url_demo() :
    response = requests.get('https://newsapi.org/v2/top-headlines?country=us&apiKey=ffeeaaeb16eb4b0b966afe73655743f6')
    data = response.json()
    articles = data.get("articles")
    for art in articles :
        print(art.get("author"))

# https://help.compose.com/docs/postgresql-and-python
def put_data_in_table_demo() :
    conn = psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')
    cur = conn.cursor()
    sql_string = """INSERT INTO practice (names)
        VALUES (%s)"""
    cur.execute(sql_string, ('Mary',))
    conn.commit()
    if conn != None:
        conn.close()

def string_to_id (s):
    if s == None:
        return -1
    if len(s) > 16:
        s = s[:16]
        print(s)
        print(int.from_bytes(s.encode(), 'little'))
    return int.from_bytes(s.encode(), 'little')

def id_to_string (n):
    if s == None:
        return -1
    return n.to_bytes(math.ceil(n.bit_length() / 8), 'little').decode()

def get_wiki_desc(name) :
    description = "No description available."
    try:
        description = wikipedia.WikipediaPage(name).summary
    except:
        pass
    return description

def add_col() :
    conn = qconn()
    cur = conn.cursor()
    sql_string = """ALTER TABLE news ADD COLUMN conflict_name text"""
    cur.execute(sql_string)
    conn.commit()
###
# End of Demo region
###

def add_images_to_conflicts() :
    conn = qconn()
    cur = conn.cursor()
    cur.execute("SELECT id FROM conflicts")
    for con_id in cur :
        img = None
        index = 0
        print('http://127.0.0.1:3003/news_conflicts/' + str(con_id[0]))
        response = requests.get('http://127.0.0.1:3003/news_conflicts/' + str(con_id[0])).json()
        for blah in response :
            img = blah.get('picture')
            print(img)
            if img != None :
                break
        if img == None :
            img = "F"
        sql_string = """UPDATE conflicts SET picture = %s WHERE id = %s"""
        cur2 = conn.cursor()
        cur2.execute(sql_string, (img,con_id,))
    conn.commit()

def add_conflictnames_to_news() :
    conn = qconn()
    cur = conn.cursor()
    cur.execute("SELECT related_conflict_id FROM news")
    for con_id in cur :
        name = None
        index = 0
        response = requests.get('http://127.0.0.1:3003/conflicts/' + str(con_id[0])).json()
        name = response[0].get('name')
        sql_string = """UPDATE news SET conflict_name = %s WHERE related_conflict_id = %s"""
        cur2 = conn.cursor()
        print(str(con_id) + " : " + str(name))
        cur2.execute(sql_string, (name,con_id,))
    conn.commit()

def get_conflicts_from_json(json_file) :
    with open(json_file, errors='ignore') as json_data:    
        data = json.load(json_data)
        return data
    return None    

def get_countries_from_json(json_file) : 
    with open(json_file, errors='ignore') as json_data:    
        data = json.load(json_data)
        return data
    return None

def put_data_in_table(sql_string, conn, data, args_tuple) :
    cur = conn.cursor()
    cur.execute(sql_string, args_tuple)
    conn.commit()

def make_conflict_args_tuple(data) :
    name = data.get("Conflict")
    global curr_conflict_id
    conflict_id = curr_conflict_id
    curr_conflict_id += 1
    deaths = data.get("Cumulative fatalities")
    recent_deaths = data.get("Fatalities in 2018")
    related_countries = data.get("Location").split(", ")
    related_news = [-1]
    year_started = data.get("Start of conflict")
    description =  get_wiki_desc(name)
    population = data.get("population")
    return (conflict_id, deaths, name, recent_deaths, related_countries, year_started, related_news, description)

def make_countries_args_tuple(data, conn) :
    name = data.get("name")
    print(name)
    global curr_country_id
    country_id = curr_country_id
    curr_country_id += 1
    region = data.get("region")
    flag = data.get("flag")
    related_conflicts = search_related_conflict(data, conn, name)
    num_ongoing_conflicts = len(related_conflicts)
    if num_ongoing_conflicts <= 0 :
        print('Excluded ' + name + '.')
        return None
    capital = data.get("capital")
    related_charities = [-1]
    description =  get_wiki_desc(name)
    population = data.get("population")
    return (country_id, name, region, num_ongoing_conflicts, capital, related_conflicts, related_charities, flag,
        description, population)

# conflict_id should be passed in by the conflict that is asking for related stories
def make_news_args_tuple(data, conn, conflict_id) :
    title = data.get("title")
    news_id = get_next_news_id(conn)
    author = data.get("author")
    description = data.get("description")
    url = data.get("url")
    picture = data.get("urlToImage")
    date_str = data.get("publishedAt")
    source = data.get("source").get("name")
    related_countries = get_countries_from_conflict(conn, conflict_id)

    return (news_id, title, picture, description, date_str, source, author, url, related_countries, 
        conflict_id)

def get_countries_from_conflict(conn, conflict_id) :
    sql = 'SELECT related_countries FROM conflicts WHERE id=%s' % (conflict_id,)
    cur = conn.cursor()
    countries_list = []
    countries_tuple = (1,)
    try:
        cur.execute(sql)
        countries_tuple = cur.fetchone()
        for x in countries_tuple :
            countries_list.append(x)
    except:
        print('No id found.')
    return countries_list

def get_next_news_id(conn) :
    cur = conn.cursor()
    cur.execute('SELECT max(id) FROM news')
    result = -1
    try:
        result = cur.fetchone()[0] + 1
    except:
        print('Warning: Failed to get new id')
    return result

def search_related_conflict(data, conn, name) :
    sql = 'SELECT id FROM conflicts WHERE related_countries && \'{%s}\'::text[]' % (name,)
    cur = conn.cursor()
    conflicts_list = []
    try:
        cur.execute(sql)
        conflicts_list = cur.fetchall()
    except:
        print('No conflicts related to: ' + name)
    return make_int_list(conflicts_list)

def put_conflicts_in_table(json_file) :
    data = get_conflicts_from_json(json_file)
    conn = psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')
    sql_string = """INSERT INTO conflicts (id, deaths, name, recent_deaths, related_countries, year_started, 
        related_news, description)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""
    for data_dict in data:
        args = make_conflict_args_tuple(data_dict)
        put_data_in_table(sql_string, conn, data_dict, args)
    if conn != None:
        conn.close()

def put_countries_in_table(json_file) :
    data = get_countries_from_json(json_file)
    sql_string = """INSERT INTO countries (id, name, region, num_ongoing_conflicts, capital, 
        related_conflicts, related_charities, flag, description, population)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
    for data_dict in data:
        # For some unknown reason, only about half the list will have conflicts counted
        # if we do not make a new connection each time. It runs slower, but works fine.
        conn = qconn()
        args = make_countries_args_tuple(data_dict, conn)
        if args != None: 
            put_data_in_table(sql_string, conn, data_dict, args)
            print('Put ' + args[1] + ' into table with conflicts = ' + str(args[3]))
        if conn != None:
            conn.close()

#keywork should be conflict name, should also pass in id of calling conflict
def put_news_in_table_for_kw(keyword, conflict_id) :
    articles = get_news_from_url(keyword)
    sql_string = """INSERT INTO news (id, title, picture, description, date_str, 
        source, author, url, related_countries, related_conflict_id)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
    for article in articles:
        # For some unknown reason, only about half the list will have conflicts counted
        # if we do not make a new connection each time. It runs slower, but works fine.
        conn = qconn()
        args = make_news_args_tuple(article, conn, conflict_id)
        if args != None: 
            put_data_in_table(sql_string, conn, article, args)
        if conn != None:
            conn.close()

def clear_table (table_to_clear):
    conn = psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')
    cur = conn.cursor()
    sql_string = """DELETE FROM %s""" % (table_to_clear,)
    cur.execute(sql_string)
    conn.commit()
    if conn != None:
        conn.close()

#https://stackoverflow.com/questions/16606357/how-to-make-a-select-with-array-contains-value-clause-in-psql
def demo_sql_1() :
    conn = psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')
    cur = conn.cursor()
    sql_string = 'SELECT * FROM conflicts WHERE related_countries && \'{Democratic Republic of the Congo}\'::text[]'
    cur.execute(sql_string)
    print(cur.fetchone())
    conn.commit()
    if conn != None:
        conn.close()

def demo_sql_2() :
    conn = psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')
    cur = conn.cursor()
    sql_string = 'SELECT * FROM conflicts WHERE related_countries && \'{Democratic Republic of the Congo}\'::text[]'
    cur.execute(sql_string)
    print(cur.fetchone())
    conn.commit()
    if conn != None:
        conn.close()

def demo_sql_3() :
    conn = psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')
    sql = 'SELECT id FROM conflicts WHERE related_countries && \'{%s}\'::text[]' % ('Yemen',)
    cur = conn.cursor()
    conflicts_list = []
    try:
        cur.execute(sql)
        conflicts_list = cur.fetchall()
        make_int_list
    except:
        pass
    result = make_int_list(conflicts_list)
    print(result)

def demo_sql_4() :
    conn = psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')
    cur = conn.cursor()
    sql_string = 'SELECT name FROM conflicts'
    cur.execute(sql_string)
    print(cur.fetchall())
    if conn != None:
        conn.close()

def demo1 () :
    with open('countries.json', errors='ignore') as json_data:    
        data = json.load(json_data)
        for country in data:
            if country.get("name") == "Yemen":
                print("hello!!!")

# Should take in a list of tuples that contain 1 int each
def make_int_list(list_arg) :
    result = []
    for x in list_arg :
        result.append(x[0])
    return result

def qconn() :
    return psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')

def put_all_conflicts() : 
    put_conflicts_in_table('conflicts_major.json')
    put_conflicts_in_table('conflicts_semimajor.json')
    put_conflicts_in_table('conflicts_small.json')
    put_conflicts_in_table('conflicts_tiny.json')

def get_news_from_url(keyword) :
    endpoint = 'https://newsapi.org/v2/everything?q=%s&apiKey=ffeeaaeb16eb4b0b966afe73655743f6' % (keyword,)
    response = requests.get(endpoint)
    data = response.json()
    return data.get("articles")

def put_all_news_related_conflicts() :
    conn = psycopg2.connect(
        host = 'armedconflicts.citm2bumyzxi.us-east-2.rds.amazonaws.com',
        port = 5432,
        user = 'armedconflicts',
        password = 'dripharder',
        database = 'ArmedConflictsDB')
    cur = conn.cursor()
    sql_string = 'SELECT name, id FROM conflicts'
    cur.execute(sql_string)
    # Note this is a list of tuples with 1 value
    list_of_conflicts = cur.fetchall()
    if conn != None:
        conn.close()
    for conflict in list_of_conflicts: 
        print('Adding news for id ' + str(conflict[1]) + ' which is ' + conflict[0])
        put_news_in_table_for_kw(conflict[0], conflict[1])

def run_main () :
    clear_table('news')
    #clear_table('countries')
    #clear_table('conflicts')
    #print("Adding Conflicts")
    #put_all_conflicts()
    #print("Adding Countries")
    #put_countries_in_table('countries.json')
    print("I read the news today oh boy...")
    put_all_news_related_conflicts()
    print("About a lucky man who made the grade...")
    print("Done.")

#https://stackoverflow.com/questions/31701991/string-of-text-to-unique-integer-method
if __name__ == "__main__":
    add_conflictnames_to_news()